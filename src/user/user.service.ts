import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';
import {compare, hash, genSalt} from 'bcryptjs'

@Injectable()
export class UserService {
    constructor(@InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>){}



    async byId(id: number){
        const user = await this.userRepository.findOne({
            where:{
                id
            },
            relations:{
                query: true
            },
            order: {
                createdAt: 'DESC'
            }
        })

        if (!user) throw new NotFoundException('Юзер не найден')
        return user
    }

    async updateProfile(id:number, dto:UserDto){
        const user = await this.byId(id)

        const isSameUser = await this.userRepository.findOneBy({email:dto.email})
        if (isSameUser && id != isSameUser.id) throw new BadRequestException('email занят')

        if(dto.password){
            const salt = await genSalt(10)
            user.password = await hash(dto.password, salt)
        }
        
        user.email = dto.email
        user.name = dto.name

        return this.userRepository.save(user)

    }

    async updateVerifiedVolonter(id: number){
        const user = await this.byId(id)
        user.volonterVerified = true
        return this.userRepository.save(user)
    }

    
    async getAll(){
        return this.userRepository.find()
    }
}
