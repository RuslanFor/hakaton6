import { Base } from "src/utils/base";
import { QueryEntity } from "src/query/query.entity";
import { Column, Entity, OneToMany } from "typeorm";



@Entity('User')
export class UserEntity extends Base {
    @Column({unique:true})
    email:string

    @Column({select:false})
    password:string

    @Column({default:''})
    name: string

    @Column({default:false, name:'is_verified'})
    volonterVerified: boolean

    @OneToMany(() => QueryEntity, query => query.user)
    query: QueryEntity[]

}