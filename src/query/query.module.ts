import { Module } from '@nestjs/common';
import { QueryService } from './query.service';
import { QueryController } from './query.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/user.entity';
import { QueryEntity } from './query.entity';


@Module({
  controllers: [QueryController],
  providers: [QueryService],
  imports:[
    TypeOrmModule.forFeature([UserEntity, QueryEntity])
  ]

})
export class QueryModule {}
