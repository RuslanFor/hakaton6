import { Body, Controller, Get, HttpCode, Param, Post, Put, Query } from '@nestjs/common';
import { QueryService } from './query.service';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { CurrentUser } from 'src/user/user.decorator';
import { CreateQueryDto } from './dto/create-query.dto';

@Controller('query')
export class QueryController {
  constructor(private readonly queryService: QueryService) {}



  @HttpCode(200)
  @Post()
  @Auth()
  async createVideo(@CurrentUser('id') id: number, @Body() dto: CreateQueryDto){
    return this.queryService.create(id,dto)
  }

  @HttpCode(200)
  @Get('/myquery')
  @Auth()
  async myquery(@CurrentUser('id') id: number){
    return this.queryService.myquery(id)
  }

  @HttpCode(200)
  @Get('/nopublic')
  async getAll(@Query('searchTerm') searchTerm?: string){
    return this.queryService.getAll(searchTerm)
  }


  @HttpCode(200)
  @Get()
  async getAllPubic(@Query('searchTerm') searchTerm?: string){
    return this.queryService.getAllPublik(searchTerm)
  }


  @HttpCode(200)
  @Get('/wefound')
  async getAllPubicWefound(@Query('searchTerm') searchTerm?: string){
    return this.queryService.getAllPublikWefound(searchTerm)
  }

  @HttpCode(200)
  @Get('/notwefound')
  async getAllPubicNotWefound(@Query('searchTerm') searchTerm?: string){
    return this.queryService.getAllPublikNotWefound(searchTerm)
  }


  @HttpCode(200)
  @Get(':id')
  async getVideo(@Param('id') id: string){
    return this.queryService.byId(+id)
  }


  @HttpCode(200)
  @Put(':id')
  @Auth()
  async updateUser(@Param('id') id: string){
    return this.queryService.updateVerifiedQuery(+id)
  }
  @HttpCode(200)
  @Put('/lol/:id')
  @Auth()
  async updateUserWefound(@Param('id') id: string){
    return this.queryService.updateVerifiedQueryWefound(+id)
  }

  @HttpCode(200)
  @Put('/status/:id')
  @Auth()
  async updateUserStatus(@Param('id') id: string){
    return this.queryService.updateVerifiedQueryStatus(+id)
  }




  
}
