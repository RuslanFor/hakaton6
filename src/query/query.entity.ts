
import { UserEntity } from "src/user/user.entity";
import { Base } from "src/utils/base";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from "typeorm";


@Entity('Query')
export class QueryEntity extends Base {

    

    @Column({default:''})
    fullname: string

    @Column({default:false, name:'is_public'})
    isPublic: boolean

    @Column({default:'', type:'text'})
    description:string

    @Column({default:'', type:'text'})
    phone:string

    @Column({default:'', name: 'thumbnail_path'})
    thumbnailPath:string

    @Column({default:'В обработке...'})
    status: string

    @Column({default: false})
    wefound: boolean

    @Column({default:'', type:'text'})
    coordinate:string

    @Column({default:'', type:'text'})
    addres:string

    @ManyToOne(() => UserEntity,user => user.query)
    @JoinColumn({name:'user_id'})
    user: UserEntity




}