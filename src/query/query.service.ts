import { Injectable, NotFoundException, Query } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QueryEntity } from './query.entity';
import { FindOptionsWhereProperty, ILike, Repository } from 'typeorm';
import { CreateQueryDto } from './dto/create-query.dto';

@Injectable()
export class QueryService {

    constructor(@InjectRepository(QueryEntity) private readonly queryRepository: Repository<QueryEntity>){}

    async create(userId: number, dto: CreateQueryDto){
        const newQuery = this.queryRepository.create({
            user: {id: userId},
            fullname: dto.fullname,
            description: dto.description,
            phone: dto.phone,
            thumbnailPath: ''
        })

        return this.queryRepository.save(newQuery)
    }

    async getAll(searchTerm?:string){

        let options:FindOptionsWhereProperty<QueryEntity> = {}

        if (searchTerm) options = {
            fullname: ILike(`%${searchTerm}%`)
        }


        return this.queryRepository.find({
            where:{
                ...options, isPublic: false
            },
            order: {
                createdAt: "DESC"
            },
            relations: {
                user: true,
            },
            select: {
                user: {
                    id: true,
                    name: true,
 
                }
            }
        })
    }
        
    async byId(id: number, isPublic = false){
        const query = await this.queryRepository.findOne({
            where: isPublic ? {id, isPublic: true} : {id},
            relations:{
                user: true,
                
            },
            select: {
                user:{
                    id:true,
                    name:true,
                    
                },
                
            }
        })

        if (!query) throw new NotFoundException(' не найдено')
        return query
    }

    async getAllPublik(searchTerm?:string){

        let options:FindOptionsWhereProperty<QueryEntity> = {}

        if (searchTerm) options = {
            fullname: ILike(`%${searchTerm}%`)
        }


        return this.queryRepository.find({
            where:{
                ...options, isPublic: true,
            },
            order: {
                createdAt: "DESC"
            },
            relations: {
                user: true,
            },
            select: {
                user: {
                    id: true,
                    name: true,
 
                }
            }
        })
    }

    async getAllPublikWefound(searchTerm?:string){

        let options:FindOptionsWhereProperty<QueryEntity> = {}

        if (searchTerm) options = {
            fullname: ILike(`%${searchTerm}%`)
        }


        return this.queryRepository.find({
            where:{
                ...options, isPublic: true, wefound: true
            },
            order: {
                createdAt: "DESC"
            },
            relations: {
                user: true,
            },
            select: {
                user: {
                    id: true,
                    name: true,
 
                }
            }
        })
    }


    async getAllPublikNotWefound(searchTerm?:string){

        let options:FindOptionsWhereProperty<QueryEntity> = {}

        if (searchTerm) options = {
            fullname: ILike(`%${searchTerm}%`)
        }


        return this.queryRepository.find({
            where:{
                ...options, isPublic: true, wefound: false
            },
            order: {
                createdAt: "DESC"
            },
            relations: {
                user: true,
            },
            select: {
                user: {
                    id: true,
                    name: true,
 
                }
            }
        })
    }


    async updateVerifiedQuery(id: number){
        const query = await this.byId(id)
        query.isPublic = true
        return this.queryRepository.save(query)
    }

    async updateVerifiedQueryWefound(id: number){
        const query = await this.byId(id)
        query.wefound = true
        return this.queryRepository.save(query)
    }
    async updateVerifiedQueryStatus(id: number){
        const query = await this.byId(id)
        query.status = 'Найден. Жив'
        return this.queryRepository.save(query)
    }


    async myquery(id:number){
        return this.queryRepository.find({
            where:{
                user:{
                    id: id
                }
            },
            relations: {
                user: true,
                
            },
            select: {
                user: {
                    id: true,
                    
                }
            }
        })
       
        
    }

}

