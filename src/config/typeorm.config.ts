import {TypeOrmModuleOptions} from '@nestjs/typeorm'
import {ConfigService} from '@nestjs/config'


export const getTypeOrmConfig = async (configService: ConfigService):Promise<TypeOrmModuleOptions> => ({
    type:'postgres',
    host:'localhost',
    port: 5432,
    database:'cool',
    username:'postgres',
    password:'rot',
    autoLoadEntities: true,
    synchronize: true
})